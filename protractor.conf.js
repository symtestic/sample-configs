// Use IIFE
(function () {

  var config = require('./protractor-default.conf.json');

  exports.config = {
    seleniumAddress: 'http://' + config.hostname + ':' + config.port + '/webdriver/' + config.protractor.username + '/' + config.protractor.password + '/wd/hub',
    specs: config.protractor.specs,
    allScriptsTimeout: config.protractor.defaultTimeout,
    jasmineNodeOpts: {
      defaultTimeoutInterval: config.protractor.defaultTimeout
    },
    multiCapabilities: config.protractor.browsers,

    onPrepare: function () {
      // The require statement must be down here, since jasmine-reporters
      // needs jasmine to be in the global and protractor does not guarantee
      // this until inside the onPrepare function.
      require('jasmine-reporters');
      jasmine.getEnv().addReporter(
          new jasmine.JUnitXmlReporter('./results/', true, true, 'summary'));

      var HtmlReporter = require('protractor-html-screenshot-reporter');

      // Add a screenshot reporter and store screenshots to `/tmp/screnshots`:
      jasmine.getEnv().addReporter(new HtmlReporter({
        baseDirectory: './results/screenshots'
      }));

      // Special output for Browser+Version XML
      var capsPromise = browser.getCapabilities();
      capsPromise.then(function (caps) {
        var browserName = caps.caps_.browserName.toUpperCase();
        var browserVersion = caps.caps_.version;
        var prePendStr = browserName + "-" + browserVersion + "-";
        jasmine.getEnv().addReporter(new
            jasmine.JUnitXmlReporter("./results/", true, true, prePendStr));
      });
    }

  };
})();

